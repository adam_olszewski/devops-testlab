# Install openssh
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
Start-sleep -s 10
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
Start-sleep -s 10
start-service sshd
Set-Service sshd -StartupType Automatic
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
start-service ssh-agent
Set-Service ssh-agent -StartupType Automatic

# PS as default shell
Set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows NT\CurrentVersion\WinLogon' -Name Shell -Value 'PowerShell.exe'


# Disable hibernation
Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Power\" -Name "HiberFileSizePercent" -Value 0
Set-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Power\" -Name "HibernateEnabled" -Value 0

# Disable sleep
powercfg /change monitor-timeout-ac 0
powercfg /change monitor-timeout-dc 0
powercfg /change disk-timeout-ac 0
powercfg /change disk-timeout-dc 0
powercfg /change standby-timeout-ac 0
powercfg /change standby-timeout-dc 0
powercfg /change hibernate-timeout-ac 0
powercfg /change hibernate-timeout-dc 0

# Disable password expiration for Administrator
Set-LocalUser Administrator -PasswordNeverExpires $true

# Configure PowerShell prompt
$psprofile = @'
Set-Location /

function prompt {
    Write-Host "[$(Get-Date -f 'HH:mm:ss')]" -ForegroundColor Yellow -NoNewline
    " PS $($executionContext.SessionState.Path.CurrentLocation)$('>' * ($nestedPromptLevel + 1)) "
}
'@

New-Item $PROFILE -ItemType File -Force
Set-Content -Path $PROFILE -Value $psprofile

