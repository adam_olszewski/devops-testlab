# Vagrant configuration for Windows Server 2019 Core

## Common used commands
```shell
vagrant reload
vagrant provision
vagrant box add ../packer-win2019/box/win2019-core-packer.box --name win2019-core --force
vagrant init win2019-core
```

## Passing raw arguments
```shell
ansible.raw_arguments = Shellwords.shellsplit(ENV["ANSIBLE_ARGS"]) if ENV["ANSIBLE_ARGS"]
$ ANSIBLE_ARGS='--ask-vault-pass' vagrant up --provision
```