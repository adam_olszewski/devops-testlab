# Ansible configuration for Windows Server 2019 Core
## Hosts file
* When using port different than 5589 (example if using Ansible with port forwarding [127.0.0.1:55895]) then scheme (http or https) must be specified:
```shell
ansible_winrm_scheme=http
```


## Common used commands
```shell
ansible win -i hosts -m win_ping
```